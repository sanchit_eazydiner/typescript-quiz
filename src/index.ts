let myQuestions = [
	{
		question: "What is 15*6?",
		answers: {
			a: '60',
			b: '75',
			c: '90'
		},
		correctAnswer: 'c'
	},
	{
		question: "What is 30/3?",
		answers: {
			a: '3',
			b: '5',
			c: '10'
		},
		correctAnswer: 'c'
	},
	{
		question: "What is the square root of 64",
		answers:{
			a:'4',
			b:'8',
			c:'16'
		},
		correctAnswer: 'b'
	}
];

let counter = 300;

function renderCounter()
{
	setInterval(function() {
	  counter--;
	   if (counter >= 0) {
		  let span = document.getElementById("counter");
		  span.innerHTML = counter.toString()+" secs remaining";
	   }
	   if (counter === 0) {
		  quizContainer.innerHTML = "sorry, out of time";
		  submitButton.style.display="none";
		  showResults(myQuestions, quizContainer, resultsContainer);
		  clearInterval(counter);
		}
		if(counter==-1)
		{
			clearInterval(counter);
			document.getElementById("counter").style.display="none";
			clearInterval(counter);
		}
	  }, 1000);

}

function startQuiz()
{
	renderCounter();
	submitButton.innerHTML = "Submit the Quiz";
    generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);


}

function generateQuiz(questions, quizContainer, resultsContainer, submitButton){

	function showQuestions(questions, quizContainer){
	let output = [];
	let answers;

	for(let i=0; i<questions.length; i++){
		
		answers = [];

		for(let letter in questions[i].answers){

			answers.push(
				'<label>'
					+ '<input type="radio" name="question'+i+'" value="'+letter+'">'
					+ letter + ': '
					+ questions[i].answers[letter]
				+ '</label>'
			);
		}

		output.push(
			'<div class="question">' + questions[i].question + '</div>'
			+ '<div class="answers">' + answers.join('') + '</div>'
		);
	}

	quizContainer.innerHTML = output.join('');
	}

	showQuestions(questions, quizContainer);

	submitButton.onclick = function(){
		counter = -1;		
		showResults(questions, quizContainer, resultsContainer);
	}
}

function showResults(questions, quizContainer, resultsContainer){
	let answerContainers = quizContainer.querySelectorAll('.answers');

let userAnswer = '';
let numCorrect = 0;

for(let i=0; i<questions.length; i++){

	userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;
	
	if(userAnswer===questions[i].correctAnswer){
		numCorrect++;
		
		answerContainers[i].style.color = 'lightgreen';
	}
	else{
		answerContainers[i].style.color = 'red';
	}
}

let len = 0;
if(questions.length!=0)
{
	len=questions.length;
}
resultsContainer.innerHTML = numCorrect + ' out of ' + len;
}

let quizContainer = document.getElementById('quiz');
let resultsContainer = document.getElementById('results');
let submitButton = document.getElementById('submit');

submitButton.onclick = function(){startQuiz();}